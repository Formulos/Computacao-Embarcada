/**
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/


#define LED_PIO
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1<<LED_PIO_PIN)
#define LED_PIO PIOC

#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

#define _PIO_DEFAULT (0u << 0)
#define _PIO_PULLUP (1u << 0)
#define _PIO_DEGLITCH (1u << 1)
#define _PIO_OPENDRAIN (1u << 2)
#define _PIO_DEBOUNCE (1u << 3)


/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/

void _pio_set(Pio *p_pio,const uint32_t ul_mask){
	p_pio->PIO_SODR = ul_mask;
	
}

void _pio_clear(Pio *p_pio, const uint32_t ul_mask){
	p_pio->PIO_CODR = ul_mask;
}

void _pio_pull_up(Pio *p_pio, const uint32_t ul_mask ,const uint32_t ul_pull_up_enable){
	p_pio->PIO_CODR = ul_mask;
	
	if (ul_pull_up_enable){
	p_pio->PIO_PUER = ul_mask;
	}
	
	else{
	p_pio->PIO_PUDR = ul_mask;
	}
	
	}
	
void _pio_set_output(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_default_level,
const uint32_t ul_multidrive_enable, const uint32_t ul_pull_up_enable){
	
	p_pio->PIO_PER = ul_mask;
	p_pio->PIO_OER = ul_mask;
	
	if(ul_default_level){
		_pio_set(p_pio, ul_mask);
		} else{
		_pio_clear(p_pio, ul_mask);
	}
	
	if(ul_multidrive_enable){
		p_pio->PIO_MDER = ul_mask;
		} else{
		p_pio->PIO_MDDR = ul_mask;
	}
	
	_pio_pull_up(p_pio, ul_mask, ul_pull_up_enable);
}



/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	sysclk_init();
	
	WDT->WDT_MR = WDT_MR_WDDIS;
	pmc_enable_periph_clk(LED_PIO_ID);
	
	pio_configure(PIOC,PIO_OUTPUT_0,LED_PIO_PIN_MASK,PIO_DEFAULT);

	// super loop
	// aplicacoes embarcadas n�o devem sair do while(1).
	while (1) {
	if(!pio_get(PIOA, PIO_INPUT, BUT_PIO_PIN_MASK)){
		for(int i = 0; i < 5; i++){
			_pio_set(PIOC, LED_PIO_PIN_MASK);
			delay_ms(100);
			_pio_clear(PIOC, LED_PIO_PIN_MASK);
			delay_ms(100);
		}
	}
	
	else {
		_pio_set(PIOC, LED_PIO_PIN_MASK);
	}
	


	}
	return 0;
}
