#include <asf.h>
#include "conf_board.h"

#define configASSERT_DEFINED 1

#define TASK_MONITOR_STACK_SIZE            (2048/sizeof(portSTACK_TYPE))
#define TASK_MONITOR_STACK_PRIORITY        (tskIDLE_PRIORITY)
#define TASK_LED_STACK_SIZE                (1024/sizeof(portSTACK_TYPE))
#define TASK_LED_STACK_PRIORITY            (tskIDLE_PRIORITY)

extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName);
extern void vApplicationIdleHook(void);
extern void vApplicationTickHook(void);
extern void vApplicationMallocFailedHook(void);
extern void xPortSysTickHandler(void);

/* Botao da placa */
#define BUT_1_PIO_ID ID_PIOD
#define BUT_1_PIO PIOD
#define BUT_1_PIN 28
#define BUT_1_PIN_MASK (1<<BUT_1_PIN)

#define BUT_2_PIO_ID ID_PIOC
#define BUT_2_PIO PIOC
#define BUT_2_PIN 31
#define BUT_2_PIN_MASK (1<<BUT_2_PIN)

#define BUT_3_PIO_ID ID_PIOA
#define BUT_3_PIO PIOA
#define BUT_3_PIN 19
#define BUT_3_PIN_MASK (1<<BUT_3_PIN)

/* leds */

#define LED_1_PIO_ID ID_PIOA
#define LED_1_PIO PIOA
#define LED_1_PIN 0
#define LED_1_PIN_MASK (1<<LED_1_PIN)

#define LED_2_PIO_ID ID_PIOC
#define LED_2_PIO PIOC
#define LED_2_PIN 30
#define LED_2_PIN_MASK (1<<LED_2_PIN)

#define LED_3_PIO_ID ID_PIOB
#define LED_3_PIO PIOB
#define LED_3_PIN 2
#define LED_3_PIN_MASK (1<<LED_3_PIN)

void but_callback1(void);
void but_callback2(void);
void but_callback3(void);
void init_but_board(void);
void pin_toggle(Pio *pio, uint32_t mask);
/** Semaforo a ser usado pela task led */
SemaphoreHandle_t xSemaphore1;
SemaphoreHandle_t xSemaphore2;
SemaphoreHandle_t xSemaphore3;

volatile int flag_tick1 = 0;
volatile int flag_tick2 = 0;
volatile int flag_tick3 = 0;
/**
 * \brief Called if stack overflow during execution
 */
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName)
{
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.
	 */
	for (;;) {
	}
}

/**
 * \brief This function is called by FreeRTOS idle task
 */
extern void vApplicationIdleHook(void)
{
	pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
}

/**
 * \brief This function is called by FreeRTOS each tick
 */
extern void vApplicationTickHook(void)
{
}

extern void vApplicationMallocFailedHook(void)
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}

/**
 * \brief This task, when activated, send every ten seconds on debug UART
 * the whole report of free heap and total tasks status
 */

void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}


static void task_monitor(void *pvParameters)
{
	static portCHAR szList[256];
	UNUSED(pvParameters);

	for (;;) {
		printf("--- task ## %u", (unsigned int)uxTaskGetNumberOfTasks());
		vTaskList((signed portCHAR *)szList);
		printf(szList);
		vTaskDelay(1000);
	}
}

/**
 * \brief This task, when activated, make LED blink at a fixed rate
 */


void LED_init(int estado){
	pmc_enable_periph_clk(LED_1_PIO_ID);
	pio_set_output(LED_1_PIO, LED_1_PIN_MASK, estado, 0, 0 );

	pmc_enable_periph_clk(LED_2_PIO_ID);
	pio_set_output(LED_2_PIO, LED_2_PIN_MASK, estado, 0, 0 );

	pmc_enable_periph_clk(LED_3_PIO_ID);
	pio_set_output(LED_3_PIO, LED_3_PIN_MASK, estado, 0, 0 );
}

static void task_led1(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	xSemaphore1 = xSemaphoreCreateBinary();
	const TickType_t xDelay = 50 /portTICK_PERIOD_MS;
	if (xSemaphore1 == NULL)
		printf("falha em criar o semaforo \n");

	for (;;) {
		if( xSemaphoreTake(xSemaphore1, ( TickType_t ) 500) == pdTRUE ){
			//pin_toggle(LED_1_PIO, LED_1_PIN_MASK);
			flag_tick1 = !flag_tick1;
		}
		if (flag_tick1 == 1){
			pin_toggle(LED_1_PIO, LED_1_PIN_MASK);
		}
	}
}

static void task_led2(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	xSemaphore2 = xSemaphoreCreateBinary();

	if (xSemaphore2 == NULL)
	printf("falha em criar o semaforo \n");

	for (;;) {
		if( xSemaphoreTake(xSemaphore2, ( TickType_t ) 500) == pdTRUE ){
			//pin_toggle(LED_2_PIO, LED_2_PIN_MASK);
			flag_tick2 = !flag_tick2;
		}
		if (flag_tick2 == 1){
			pin_toggle(LED_2_PIO, LED_2_PIN_MASK);
			delay_ms(100);
		}
	}
}

static void task_led3(void *pvParameters){
	/* Attempt to create a semaphore. */
	xSemaphore3 = xSemaphoreCreateBinary();

	if (xSemaphore3 == NULL)
	printf("falha em criar o semaforo \n");

	for (;;) {
		if( xSemaphoreTake(xSemaphore3, ( TickType_t ) 500) == pdTRUE ){
			//pin_toggle(LED_3_PIO, LED_3_PIN_MASK);
			flag_tick3 = !flag_tick3;
			}
		if (flag_tick3 == 1){
			pin_toggle(LED_3_PIO, LED_3_PIN_MASK);
			delay_ms(250);
			}
		}

}

void but_callback1(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but_callback1 \n");
	xSemaphoreGiveFromISR(xSemaphore1, &xHigherPriorityTaskWoken);
	printf("semafaro1 tx \n");
}

void but_callback2(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but_callback2 \n");
	xSemaphoreGiveFromISR(xSemaphore2, &xHigherPriorityTaskWoken);
	printf("semafaro2 tx \n");
}

void but_callback3(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but_callback3 \n");
	xSemaphoreGiveFromISR(xSemaphore3, &xHigherPriorityTaskWoken);
	printf("semafaro3 tx \n");
}

void init_but_board(void){
	/* conf bot�o 1 da placa como entrada */
	NVIC_EnableIRQ(BUT_1_PIO_ID);
	NVIC_SetPriority(BUT_1_PIO_ID, 8);
	pio_configure(BUT_1_PIO, PIO_INPUT, BUT_1_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT_1_PIO, BUT_1_PIN_MASK, 60);
	pio_enable_interrupt(BUT_1_PIO, BUT_1_PIN_MASK);
	pio_handler_set(BUT_1_PIO, BUT_1_PIO_ID, BUT_1_PIN_MASK, PIO_IT_FALL_EDGE , but_callback1);

	/* conf bot�o 2 da placa como entrada */
	NVIC_EnableIRQ(BUT_2_PIO_ID);
	NVIC_SetPriority(BUT_2_PIO_ID, 9);
	pio_configure(BUT_2_PIO, PIO_INPUT, BUT_2_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT_2_PIO, BUT_2_PIN_MASK, 60);
	pio_enable_interrupt(BUT_2_PIO, BUT_2_PIN_MASK);
	pio_handler_set(BUT_2_PIO, BUT_2_PIO_ID, BUT_2_PIN_MASK, PIO_IT_FALL_EDGE , but_callback2);

	/* conf bot�o 3 da placa como entrada */
	NVIC_EnableIRQ(BUT_3_PIO_ID);
	NVIC_SetPriority(BUT_3_PIO_ID, 10);
	pio_configure(BUT_3_PIO, PIO_INPUT, BUT_3_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT_3_PIO, BUT_3_PIN_MASK, 60);
	pio_enable_interrupt(BUT_3_PIO, BUT_3_PIN_MASK);
	pio_handler_set(BUT_3_PIO, BUT_3_PIO_ID, BUT_3_PIN_MASK, PIO_IT_FALL_EDGE , but_callback3);
}





/**
 * \brief Configure the console UART.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#if (defined CONF_UART_CHAR_LENGTH)
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#if (defined CONF_UART_STOP_BITS)
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};

	/* Configure console UART. */
	stdio_serial_init(CONF_UART, &uart_serial_options);

	/* Specify that stdout should not be buffered. */
#if defined(__GNUC__)
	setbuf(stdout, NULL);
#else
	/* Already the case in IAR's Normal DLIB default configuration: printf()
	 * emits one character at a time.
	 */
#endif
}

/**
 *  \brief FreeRTOS Real Time Kernel example entry point.
 *
 *  \return Unused (ANSI-C compatibility).
 */
int main(void){
	/* Initialize the SAM system */
	sysclk_init();
	board_init();

	/* Initialize the console uart */
	configure_console();

	/* Output demo information. */
	printf("-- Freertos Example --\n\r");
	printf("-- %s\n\r", BOARD_NAME);
	printf("-- Compiled: %s %s --\n\r", __DATE__, __TIME__);

	/* iniciliza botao */
	init_but_board();

	LED_init(0);
	flag_tick1 = 0;
	flag_tick2 = 0;
	flag_tick3 = 0;
	
	/* Create task to monitor processor activity */
	if (xTaskCreate(task_monitor, "Monitor", TASK_MONITOR_STACK_SIZE, NULL,
			TASK_MONITOR_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create Monitor task\r\n");
	}

	/* Create task to make led blink */
	if (xTaskCreate(task_led1, "Led1", TASK_LED_STACK_SIZE, NULL,
			TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	if (xTaskCreate(task_led2, "Led2", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	if (xTaskCreate(task_led3, "Led3", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}

	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle task. */
	return 0;
}
