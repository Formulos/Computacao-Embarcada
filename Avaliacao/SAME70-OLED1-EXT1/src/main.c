/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */

/*
Instru�oes:

bot�o 1 alarme aumenta em 1 minuto
bot�o 2 desativa o timer, ele pode ser ativado novamente apertando o bot�o 1
*/
#include <asf.h>

#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"

#define YEAR        2018
#define MOUNTH      4
#define DAY         19
#define WEEK        12
#define HOUR        11

#define SECOND      0

/**
* LED
*/
#define LED_PIO_ID	   ID_PIOC
#define LED_PIO        PIOC
#define LED_PIN		   8
#define LED_PIN_MASK   (1<<LED_PIN)

#define LED1_PIO_ID	   ID_PIOA
#define LED1_PIO        PIOA
#define LED1_PIN		   0
#define LED1_PIN_MASK   (1<<LED1_PIN)

#define LED2_PIO_ID	   ID_PIOC
#define LED2_PIO        PIOC
#define LED2_PIN		   30
#define LED2_PIN_MASK   (1<<LED2_PIN)

#define LED3_PIO_ID	   ID_PIOB
#define LED3_PIO        PIOB
#define LED3_PIN		   2
#define LED3_PIN_MASK   (1<<LED3_PIN)
/**
* Bot�o
*/
#define BUT_PIO_ID			  ID_PIOA
#define BUT_PIO				  PIOA
#define BUT_PIN				  11
#define BUT_PIN_MASK			  (1 << BUT_PIN)
#define BUT_DEBOUNCING_VALUE  79

#define BUT1_PIO_ID			  ID_PIOD
#define BUT1_PIO				  PIOD
#define BUT1_PIN				  28
#define BUT1_PIN_MASK			  (1 << BUT1_PIN)
#define BUT1_DEBOUNCING_VALUE  79

#define BUT2_PIO_ID			  ID_PIOC
#define BUT2_PIO				  PIOC
#define BUT2_PIN				  31
#define BUT2_PIN_MASK			  (1 << BUT2_PIN)
#define BUT2_DEBOUNCING_VALUE  79

#define BUT3_PIO_ID			  ID_PIOA
#define BUT3_PIO				  PIOA
#define BUT3_PIN				  19
#define BUT3_PIN_MASK			  (1 << BUT3_PIN)
#define BUT3_DEBOUNCING_VALUE  79

/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

volatile uint8_t flag_led0 = 1;
volatile uint8_t flag_alarm = 1;

volatile uint8_t flag_led1 = 1;
volatile uint8_t flag_but1 = 1;

volatile uint8_t flag_led2 = 1;
volatile uint8_t flag_but2 = 1;

volatile uint8_t flag_led3 = 1;
volatile uint8_t flag_but3 = 1;

volatile int timer = 0;
volatile int start_minute = 12;
volatile int hour,minute,second;

void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

//interrupt
static void Button1_Handler(uint32_t id, uint32_t mask)
{
	timer += 1;
	//pio_clear(LED1_PIO,LED1_PIN_MASK);
	flag_but1 = 1;
	pio_clear(LED1_PIO, LED1_PIN_MASK);
	/*
	if (timer >1){
	pio_clear(LED2_PIO, LED2_PIN_MASK);}
	
	if (timer >2){
	pio_clear(LED3_PIO, LED3_PIN_MASK);}*/
	
}

static void Button2_Handler(uint32_t id, uint32_t mask)
{
	flag_but1 = 0;
	pio_set(LED_PIO, LED_PIN_MASK);
	timer = 0;
	start_minute = minute;
	
	pio_set(LED1_PIO, LED1_PIN_MASK);
	//pio_set(LED2_PIN, LED2_PIN_MASK);
	//pio_set(LED3_PIO, LED3_PIN_MASK);
}

void BUT_init(void){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT1_PIO_ID);
	pio_set_input(BUT1_PIO, BUT1_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrupo */
	pio_enable_interrupt(BUT1_PIO, BUT1_PIN_MASK);// INTERRUPCAO
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN_MASK, PIO_IT_FALL_EDGE, Button1_Handler);
	
	/* habilita interrupco do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 1);
	
	//BOTAO 2
		
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT2_PIO_ID);
	pio_set_input(BUT2_PIO, BUT2_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrupo */
	pio_enable_interrupt(BUT2_PIO, BUT2_PIN_MASK);// INTERRUPCAO
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN_MASK, PIO_IT_FALL_EDGE, Button2_Handler);

	/* habilita interrupco do PIO que controla o botao */
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 1);
}

void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora*/
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, start_minute, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao*/
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN);

}
void LED_init(int ticktack){
	pmc_enable_periph_clk(LED_PIO_ID);
	pio_set_output(LED_PIO, LED_PIN_MASK, ticktack, 0, 0 );
	
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIN_MASK, ticktack, 0, 0 );

	
	pmc_enable_periph_clk(LED2_PIO_ID);
	pio_set_output(LED2_PIO, LED2_PIN_MASK, ticktack, 0, 0 );

	
	pmc_enable_periph_clk(LED3_PIO_ID);
	pio_set_output(LED3_PIO, LED3_PIN_MASK, ticktack, 0, 0 );

};

void TC0_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrupo foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 0);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);
	

	/** Muda o estado do LED */
	if((flag_but1) && (minute - start_minute >= timer) && (timer != 0)){
			pin_toggle(LED_PIO, LED_PIN_MASK);
	}
}

void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int frequencia){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();


	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrupco no RC compare */
	tc_find_mck_divisor(frequencia, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / frequencia);

	/* Configura e ativa interrupco no TC canal 0 */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}

/**
* Configura TimerCounter (TC) para gerar uma interrupcao no canal (ID_TC e TC_CHANNEL)
*/

int main (void)
{
	RTC_init();
	
	BUT_init();
	
	LED_init(0);
	
	TC_init(TC0, ID_TC0, 0, 8);
	
	char S[150]; //string do que vai ser escrito na placa

	
	
	board_init();
	sysclk_init();
	delay_init();
	gfx_mono_ssd1306_init();
	
	timer = 0;
	hour = 0;
	second = 0;
	minute = 0;
	start_minute = 12;
	
	
	flag_led0 = 0;
	flag_alarm = 0;
	
	flag_led1 = 0;
	flag_but1 = 1;
	
	flag_led2 = 0;
	flag_but2 = 0;
	
	flag_led3 = 0;
	flag_but3 = 0;
	
	pio_set(LED1_PIO, LED1_PIN_MASK);
	pio_set(LED2_PIO, LED2_PIN_MASK);
	pio_set(LED3_PIO, LED3_PIN_MASK);
	
	while(1) {
			gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
			rtc_get_time(RTC, &hour, &minute, &second);
			sprintf( S,"%d:%d", hour, minute );
			gfx_mono_draw_string(S, 0, 0, &sysfont);
			/*
			if ((minute - start_minute >= timer) && (timer != 0)) {
				pin_toggle(LED_PIO, LED_PIN_MASK);
			}*/
			
			delay_ms(60000); // 1 minuto
	
	}
}
